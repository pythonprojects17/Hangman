# to exit if the user won
import sys

MAX_TRIES = 6
HANGMAN_ASCII_ART = '''  _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \\ / _` | '_ ` _ \\ / _` | '_ \\
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\\__,_|_| |_|\\__, |_| |_| |_|\\__,_|_| |_|
                      __/ |                      
                     |___/'''
picture_1 = '    x-------x'
picture_2 = '''    x-------x
    |
    |
    |
    |
    |
'''
picture_3 = '''    x-------x
    |       |
    |       0
    |
    |
    |'''
picture_4 = '''    x-------x
    |       |
    |       0
    |       |
    |
    |'''
picture_5 = '''    x-------x
    |       |
    |       0
    |      /|\\
    |
    |'''
picture_6 = '''    x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |'''
picture_7 = '''    x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |'''


def start():
    # print the start msg
    print("%s\n%d" % (HANGMAN_ASCII_ART, MAX_TRIES))


def show_hidden_word(secret_word, old_letters_guessed):
    """
    This function get word and return a string of the word, without the letters that don't in the list
    :param secret_word: a secret word (type: str)
    :param old_letters_guessed: a list of old letters (type: list)
    :return: a string (rtype: str)
    """
    temp = ""

    for i in secret_word:
        if i in old_letters_guessed:
            temp += i + " "
            continue
        temp += '_ '
    return temp


def print_old_letters(letters):
    """
    The function prints X and then the sorted letters in the array letters with "->" between them.
    :param letters: A list of letters (type: list)
    :return: None (rtype: None)
    """
    print("X\n%s" % " -> ".join(sorted(letters)))


def print_hangman(num_of_tries):
    """
    The function get the num of tries and print the current hangman, if the tries ends, func return False
    :param num_of_tries:
    :return: bool
    """
    states = {1: picture_2, 2: picture_3, 3: picture_4, 4: picture_5, 5: picture_6, 6: picture_7}

    # The hangman is hang
    if num_of_tries == MAX_TRIES:
        print("\n" + states[num_of_tries])
        return False

    print(":(\n" + states[num_of_tries])
    return True


def check_valid_input(letter_guessed, old_letters_guessed):
    """
    The function get letter and check if it only one letter and if he alpha character and if he does not appear inn old_letters_guessed
    :param letter_guessed: the str to check (type: str)
    :param old_letters_guessed: the list of the old letters (list)
    :return: True - > if the letter ok, else -> False (rtype: bool)"""

    letter_guessed = letter_guessed.lower()
    if len(letter_guessed) > 1 or not(letter_guessed.isalpha()):
        print("X")
        return False

    if letter_guessed in old_letters_guessed:
        print_old_letters(old_letters_guessed)
        return False

    old_letters_guessed.append(letter_guessed)
    return True


def choose_word(file_path, index):
    """
    This function get path and index and return the index of the word in the index
    also, the len of all the word (any word, that appear more then one time
    counting like 1)
    :param file_path: the path of the file (type: str)
    :param index: the index of the word (type: int)
    :return: tuple - index 0: the len of the word (count like one) (rtype: tuple)
    """
    data = open(file_path, 'r').read().split(" ")
    return data[(index % len(data)) - 1]


def check_win(secret_word, old_letters_guessed):
    """
    The function get the secret word and the old letters and check if the secret word in the old letters
    :param secret_word: the word
    :param old_letters_guessed: the old letters
    :return: bool
    """
    for i in sorted(secret_word):
        if not(i in old_letters_guessed):
            return False
    return True


def main():
    old_letters_guessed = []
    num_of_tries = 0

    start()
    path = input("Enter file path: ")
    dex = int(input("Enter index: "))
    secret_word = choose_word(path, dex)

    print("\nLet\'s start!\n" + picture_1)
    print(show_hidden_word(secret_word, old_letters_guessed))

    while True:
        get = input("Guess a letter: ")

        if not(check_valid_input(get, old_letters_guessed)):
            continue

        if check_win(secret_word, old_letters_guessed):
            print(show_hidden_word(secret_word, old_letters_guessed))
            sys.exit('WIN')

        elif not(get.lower() in secret_word.lower()):
            num_of_tries += 1

            if not(print_hangman(num_of_tries)):
                print(show_hidden_word(secret_word, old_letters_guessed))
                break

        print(show_hidden_word(secret_word, old_letters_guessed))

    print("LOSE")


if __name__ == '__main__':
    main()
